# PWP SPRING 2020
# Web App Store
# Group information
* Student 1. Nikunj Arora - narora19@student.oulu.fi
* Student 2. Dennis Goyal - dgoyal19@oulu.fi
* Student 3. Yar Zar Moe Myint - ymoemyin19@student.oulu.fi

__Remember to include all required documentation and HOWTOs, including how to create and populate the database, how to run and test the API, the url to the entrypoint and instructions on how to setup and run the client__


## Setup

- Clone the repository  
`git clone https://gitlab.com/nikunjarora12345/pwp-.git`

- Switch to dev branch  
`git checkout dev`

- Download the .env file into the root directory of the project

- Install node modules  
`npm install`

- To run development server  
`npm run devStart`

- To build the client  
`npm run build`

- To run production server  
`npm run start`

- To run tests  
`npm run test`

- Deploy to heroku   
`sudo snap install heroku --classic`   
`heroku login`   
`heroku git:remote -a pwa-store`   
`git push heroku dev:master`   

## Online hosted address

[Pwa Store](https://pwa-store.herokuapp.com)

[Documentation](https://pwa-store.herokuapp.com/api-docs)
