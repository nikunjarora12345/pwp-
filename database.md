# Database installation guide

**Database Type**: NoSQL

**Database Used**: [MongoDB](https://www.mongodb.com/)

**Backend Technology Used**: [NodeJs](https://nodejs.org/en/)

**Database API Used**: [Mongoose](https://www.npmjs.com/package/mongoose)

**Database Hosting Service**: [MLab](https://mlab.com/)

* For installation, we use the node package manager(npm).

  `npm install mongoose`
  
* Then edit the url of the database inside config/keys.js. Edit the value of the mongoURI variable.

  ```javascript
  module.exports = {
	'mongoURI': `mongodb://${process.env.dbUsername}:${process.env.dbPassword}@ds056559.mlab.com:56559/webappstoredev`
  };
  ```

